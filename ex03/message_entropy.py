# -*- coding: utf-8 -*-

from math import log

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


def letter_freq(message):
    return [(float(message.count(chr(code)))) for code in range(256) if chr(code) in message]


def entropy(message):
    freqs = letter_freq(message)
    length = len(message)

    return sum(([(abs(freqs[bit] / length * log(freqs[bit] / length, 2))) for bit in range(len(freqs))]))

if __name__ == "__main__":
    for msg in '', 'aaaa', 'aaba', 'abcd', 'This is a short text.':
        print "{:20}: {:10.3f} bits".format(msg, entropy(msg))

# -*- coding: utf-8 -*-


__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


def bubble_sort(data):
    data = list(data)

    for n in range(len(data) - 1, 0, -1):
        for pos in range(n):
            if data[pos + 1] < data[pos]:
                data[pos], data[pos + 1] = data[pos + 1], data[pos]

    return data


if __name__ == "__main__":
    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print "{:30} --> {:30}".format(data, bubble_sort(data))

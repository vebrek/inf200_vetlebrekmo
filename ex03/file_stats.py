# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


def char_counts(filename):
    FILESTRING = str(open(filename, 'rb').read())
    return [(FILESTRING.count(chr(code))) for code in range(256)]


if __name__ == '__main__':
    fname = 'file_stats.py'
    freqs = char_counts(fname)

    for code in range(256):
        if freqs[code] > 0:
            print('{:3}{:10}'.format(code, freqs[code]))
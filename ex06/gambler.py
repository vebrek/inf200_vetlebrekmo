# -*- coding: utf-8 -*-

import random

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


class Game(object):
    def __init__(self, money, total_amount, probability):
        self.m = money
        self.M = total_amount
        self.p = probability

    def flip_coin(self):
        self.m += self.win()

    def win(self):
        return (random.random() < self.p) * 2 - 1

    def end_game(self):
        return False if (self.m == self.M or self.m == 0) else True

    def gambler_win(self):
        return True if self.m == self.M else False


class Simulation(object):
    def __init__(self, money, total_amount, probability):
        self.m = money
        self.M = total_amount
        self.p = probability

        self.duration = []
        self.gambler_win = 0
        self.length_triumph = []
        self.length_ruin = []

    """
        I don't know why this method had to be static or what it mean,
        but pyCharm tells me I have to do it this way
    """

    @staticmethod
    def single_game(game):
        flips = 0
        while game.end_game():
            flips += 1
            game.flip_coin()
        return flips

    def duration_per_type(self, winner, flips):
        if winner:
            self.length_triumph.extend([flips])
        else:
            self.length_ruin.extend([flips])

    def game_duration(self, flips):
        self.duration.extend([flips])

    def run_simulation(self, simulations):
        for _ in range(simulations):
            game = Game(self.m, self.M, self.p)
            simulation = self.single_game(game)
            self.duration_per_type(game.gambler_win(), simulation)
            self.game_duration(simulation)


if __name__ == '__main__':
    games = 20
    M = 100
    m = 25
    probabilities = [0.0, 0.1, 0.2, 0.4, 0.45, 0.49, 0.5, 0.9]

    for p in probabilities:
        sim = Simulation(m, M, p)
        sim.run_simulation(games)

        print "Probability: {}".format(p)
        print "Game Lengths: {}".format(sim.duration)
        print "Gambler Wins: {}".format(len(sim.length_triumph))
        print "Gambler Wins Lengths: {}".format(sim.length_triumph)
        print "Bank Wins: {}".format(len(sim.length_ruin))
        print "Bank Wins Lengths: {}".format(sim.length_ruin)
        print "\n"

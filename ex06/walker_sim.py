# -*- coding: utf-8 -*-

import random

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


class Walker(object):
    def __init__(self, start, home):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        """
        self.home = home
        self._steps = 0
        self._pos = start

    def get_position(self):
        """Returns current position."""
        return self._pos

    def is_at_home(self):
        """Returns True if walker is at home position."""
        return self._pos == self.home

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """
        self._steps += 1
        self._pos += random.randint(0, 1) * 2 - 1

    def num_steps(self):
        """Returns number of steps"""
        return self._steps


class Simulation(object):
    def __init__(self, start, home, seed=12345):
        self._start = start
        self._home = home
        self.seed = seed

    def single_walk(self):
        walker = Walker(self._start, self._home)

        while not(walker.is_at_home()):
            walker.move()
        return walker.num_steps()

    def run_simulation(self, sims):
        random.seed(self.seed)
        return [self.single_walk() for _ in range(sims)]


if __name__ == '__main__':
    walks = 20
    simulations = 6
    seeds = [12345, 54321]

    for sim in range(simulations):
        w_start = 10 * ((sim % 6) / 3)
        w_home = 10 * (((5 - sim) % 6) / 3)
        w_seed = seeds[(sim % 3) / 2]
        simulation = Simulation(w_start, w_home, seed=w_seed)

        print "Seed: {}, Start: {}, Home: {}".format(w_seed, w_start, w_home)
        print "{:3} \n".format(simulation.run_simulation(walks))

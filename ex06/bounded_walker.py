# -*- coding: utf-8 -*-

import random

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


class Walker(object):
    def __init__(self, start, home, left_limit, right_limit):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        """
        self.home = home
        self._steps = 0
        self._left_limit = left_limit
        self._right_limit = right_limit
        self._pos = start

    def get_position(self):
        """Returns current position."""
        return self._pos

    def is_at_home(self):
        """Returns True if walker is at home position."""
        return self._pos >= self.home

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """
        new_pos = self._pos + random.randint(0, 1) * 2 - 1

        if self._left_limit <= new_pos <= self._right_limit:
            self._pos = new_pos
            self._steps += 1

    def num_steps(self):
        """Returns number of steps"""
        return self._steps


class Simulation(object):
    def __init__(self, start, home, left_limit, right_limit, seed=123456):

        if start < left_limit or start > right_limit:
            raise ValueError('Start is outside of limits')
        if home < left_limit or start > right_limit:
            raise ValueError('Home is outside of limits')

        self.seed = seed
        self._start = start
        self._home = home
        self._left_limit = left_limit
        self._right_limit = right_limit

    def single_walk(self):
        walker = Walker(self._start, self._home,
                        self._left_limit, self._right_limit)

        while not(walker.is_at_home()):
            walker.move()
        return walker.num_steps()

    def run_simulation(self, sims):
        random.seed(self.seed)
        return [self.single_walk() for _ in range(sims)]


if __name__ == '__main__':
    walks_per_lim = 20
    w_start = 0
    w_home = 20
    left_lim = [0, -10, -100, -1000, -10000]
    right_lim = 20
    for l_lim in left_lim:
        simulation = Simulation(w_start, w_home, l_lim, right_lim)

        print "Left Boundary: {:6}".format(l_lim)
        print "{:3} \n".format(simulation.run_simulation(walks_per_lim))

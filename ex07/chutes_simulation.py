# -*- coding: utf-8 -*-

"""
Chutes & Ladders: A fully object-oriented simulation setup.
"""


import random
import json
import itertools as it

__author__ = 'Vetle Brekmo & Bjørn Lie Rapp'
__email__ = 'vetlebr@nmbu.no & bjra@nmbu.no'


class Board(object):
    """
    Represents game board.
    """
    with open('default_board.json') as default_file:
        default = json.load(default_file)

    def __init__(self, ladders=None, chutes=None, goal=None):
        """
        :param ladders: list of (start, end) tuples representing ladders
        :param chutes: list of (start, end) tuples representing chutes
        :param goal: destination square
        """
        self.goal = goal if goal else Board.default['goal']
        ladders = (ladders if ladders is not None else
                   tuple(Board.default['ladders']))
        chutes = (chutes if chutes is not None else
                  tuple(Board.default['chutes']))

        self.adjustments = {s: e-s for s, e in it.chain(ladders, chutes)}

    def goal_reached(self, player):
        """
        Returns True if player has reached goal.

        :param player: Player instance
        """
        return player.pos >= self.goal

    def position_adjustment(self, position):
        """
        Return change to position due to chute or ladder.

        If the player is not at the start of a chute/ladder, it returns 0.

        :param position: Player position
        :returns: number of fields player must move to get to correct position
        """
        return self.adjustments.get(position, 0)


class Player(object):
    """
    Implements single player.
    """
    player_type = "Nomal"

    def __init__(self, board):
        """
        :param board: board on which player is living
        """
        self.board = board
        self.pos = 0

    def move(self):
        """
        Moves player to new position.
        """
        self.pos += random.randint(1, 6)
        self.pos += self.board.position_adjustment(self.pos)


class ResilientPlayer(Player):
    """
    Implements a player who makes extra efforts after sliding down.

    At the step after sliding down a slide, this player moves
    extra_steps squares more than the result of the die cast
    at the next move.
    """
    player_type = "Resillient"

    def __init__(self, board, extra_steps=1):
        """
        :param extra_steps: number of extra steps on move after sliding down
        """
        Player.__init__(self, board)
        self.extra_steps = extra_steps
        self.dropped = False

    def move(self):
        self.pos += random.randint(1, 6) + self.dropped * self.extra_steps

        adj = self.board.position_adjustment(self.pos)

        self.dropped = True if adj < 0 else False
        self.pos += adj


class LazyPlayer(Player):
    """
    Implements a player who makes a lesser effort after climbing up.

    At the step after climbing a slide, this player moves
    dropped_steps squares less than the result of the die cast
    at the next move (but never backward).
    """
    player_type = "Lazy"

    def __init__(self, board, dropped_steps=1):
        """
        :param dropped_steps: number of steps dropped after climbing up
        """
        Player.__init__(self, board)
        self.dropped_steps = dropped_steps
        self.climbed = False

    def move(self):
        self.pos += max(random.randint(1, 6) -
                        self.climbed * self.dropped_steps, 0)

        adj = self.board.position_adjustment(self.pos)

        self.climbed = True if adj > 0 else False
        self.pos += adj


class Simulation(object):
    """
    Implements a complete Chutes & Ladders simulation.
    """

    def __init__(self, player_field, board=None, seed=1234567,
                 randomize_players=False):
        """
        :param player_field: list of player classes, one per player to use
        :param board: Board instance (default: standard board)
        :param seed: random generator seed
        :param randomize_players: randomize player order at start of game
        """
        self.player_field = player_field
        self.seed = seed
        self.board = board if board else Board()
        self.randomize = randomize_players
        self.result = []

    def single_game(self):
        """
        Returns winner type and number of steps for single game.

        :returns: (number_of_steps, winner_class) tuple
        """
        if self.randomize:
            random.shuffle(self.player_field)

        players = [player(self.board) for player in self.player_field]

        dice_trow = 0
        while True:
            turn = dice_trow % len(players)
            players[turn].move()
            dice_trow += 1

            if self.board.goal_reached(players[turn]):
                return dice_trow/len(players) + 1, self.player_field[turn]

    def run_simulation(self, num_games):
        """
        Run a set of games, store results in Simulation.

        If results exist from before, new data will be added to existing data.

        :param num_games: number of games to play
        """
        self.result.extend([self.single_game() for _ in range(num_games)])

    def players_per_type(self):
        """
        Returns a dict mapping player classes to number of players.
        """

        return {k: self.player_field.count(k) for k in set(self.player_field)}

    def winners_per_type(self):
        """
        Returns dict showing number of winners for each type of player.
        """
        if not self.result:
            raise RuntimeError(u'Ingen resultater enda.'
                               u'Venligst kjør minst en simulering først')

        winners = [w[1] for w in self.result]
        return {k: winners.count(k) for k in set(self.player_field)}

    def durations_per_type(self):
        """
        Returns dict mapping winner type to list of game durations for type.
        """
        if not self.result:
            raise RuntimeError(u'Ingen resultater enda.'
                               u'Venligst kjør minst en simulering først')

        length = {p: [] for p in set(self.player_field)}
        for (l, w) in self.result:
            length[w].append(l)

        return length


if __name__ == '__main__':
    # The following code should work (and print something else than None)

    print '**** First Simulation: Single player, standard board ****'
    sim = Simulation([Player])
    print sim.single_game()
    sim.run_simulation(10)
    print sim.players_per_type()
    print sim.winners_per_type()
    print sim.durations_per_type()

    print '\n**** Second Simulation: Four players, standard board ****'
    sim = Simulation([Player, Player, LazyPlayer, ResilientPlayer])
    print sim.single_game()
    sim.run_simulation(10)
    print sim.players_per_type()
    print sim.winners_per_type()
    print sim.durations_per_type()

    print '\n**** Third Simulation: Four players, small board ****'
    my_board = Board(ladders=[(3, 10), (5, 8)], chutes=[(9, 2)], goal=20)
    sim = Simulation([Player, Player, LazyPlayer, ResilientPlayer],
                     board=my_board)
    print sim.single_game()
    sim.run_simulation(10)
    print sim.players_per_type()
    print sim.winners_per_type()
    print sim.durations_per_type()

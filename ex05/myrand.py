# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


class LCGRand(object):
    def __init__(self, seed):
        self.a = 7 ** 5
        self.m = 2**31 - 1
        self.rand_num = seed

    def rand(self):
        self.rand_num = self.a * self.rand_num % self.m
        return self.rand_num

    pass


class ListRand(object):
    def __init__(self, rand_list):
        self.r = rand_list
        self.n = -1
        pass

    def rand(self):
        self.n += 1
        if self.n >= len(self.r):
            raise RuntimeError("No more numbers left")

        return self.r[self.n]


if __name__ == '__main__':
    lcg = LCGRand(3235909203285)
    num = ListRand([213, 12784, 54, 345, 4564741, 43, 46, 2, 3, 6])

    print "       LCG              List"
    for i in range(0, 10):
        print "{:10}   {:15}".format(lcg.rand(), num.rand())

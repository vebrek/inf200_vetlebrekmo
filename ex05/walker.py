# -*- coding: utf-8 -*-

import random as rand

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


class Walker(object):
    def __init__(self, start, home):
        self.home = home
        self.steps = 0
        self._pos = start

    def take_one_step(self):
        self.steps += 1
        self._pos += rand.randint(0, 1) * 2 - 1

    def is_home(self):
        return self.stud_pos() == self.home

    def stud_pos(self):
        return self._pos

    def num_steps(self):
        return self.steps


def stats(distance):
    sims = 5
    path = [0] * sims

    for i in range(0, sims):
        student = Walker(0, distance)
        while not(student.is_home()):
            student.take_one_step()
        path[i] = student.steps

    return path

if __name__ == '__main__':
    for d in ([1, 2, 5, 10, 20, 50, 100]):
        print "Distance: {:3} -> Path lengths: {}".format(d, stats(d))

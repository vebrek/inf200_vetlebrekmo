# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'

SUITS = ('C', 'S', 'H', 'D')
VALUES = range(1, 14)


def deck_loop():
    deck = []
    for suit in SUITS:
        for val in VALUES:
            deck.append((suit, val))
    return deck


def deck_comp():
    return [(suit, val) for suit in SUITS for val in VALUES]        #Used list comprehension to make same result as deck_loop() and return it


if __name__ == '__main__':
    if deck_loop() != deck_comp():
        print "ERROR!"
    else:                                                           #Confirmation of code is working
        print "SUCCESS!"
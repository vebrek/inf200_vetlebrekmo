# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


def squares_by_loop(n):                             #Function to square numbers that have the reminder of 1, when divided by 3
    list = []                                       #List to hold squared numbers
    for k in range(n):
        if k % 3 == 1:                              #Check if k / 3 have the rest of one
            list.extend([k**2])                     #Add k to list
    return list


def squares_by_comp(n):
    return [k**2 for k in range(n) if k % 3 == 1]


if __name__ == '__main__':
    n = 10                                          #Number to be used by fnuctions
    if squares_by_loop(n) != squares_by_comp(n):
        print "ERROR!"
    else:                                           #Confirmation of code is working
        print "SUCCESS!"
# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


from random import randint as dices

"""
This is a game where you have to guess the sum of two random numbers between 1 and 6. It could
be for instance two dices. You have 3 tries to guess the right sum.
"""


def player_guess():
    guess = 0
    while guess < 1:                                        #Wait until player have made a guess
        guess = int(raw_input('Your guess: '))
    return guess


def dice():
    return dices(1, 6) + dices(1, 6)                        #Generates two random numbers and add them together


def check_ans(ans, guess):                                  #Check if player guessed the right number
    return ans == guess

if __name__ == '__main__':
    victory = False
    tries = 3                                               #Number of tries by default
    ans = dice()

    while not victory and tries > 0:
        guess = player_guess()                              #Variable to hold players guess
        victory = check_ans(ans, guess)                     #Checks if player guessed the right number
        if not victory:
            print 'Wrong, try again!'
            tries -= 1                                      #Guessed wrong and used one try

    if tries > 0:                                           #Final check if player won or not.
        print 'You won ' , tries , ' points.'               #Points calculated by how many points left
    else:
        print 'You lost. Correct answer:' , ans , '.'
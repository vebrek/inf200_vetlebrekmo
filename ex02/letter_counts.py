# -*- coding: utf-8 -*-

__author__ = 'Vetle Brekmo'
__email__ = 'vetlebr@nmbu.no'


def letter_freq(text):
    letter = []
    count = []
    order = ''.join(sorted(text.lower()))                   #Variable of type string that holds letters from text sorted alphabetically
    n = 0                                                   #Variable to keep track on how many letters have been counted

    for x in order:
        if n == 0:                                          #Catch the first letter to avoid negatvie index for order in next
            if x != ' ':                                    #if statemen
                letter.extend([x])
            else:
                letter.extend(["Space"])                    #Make spaces more readable
            count.extend([order.count(x)])
        elif x != order[n - 1]:                             #Only count every letter once. Order is sorted so the letter
            letter.extend([x])                              #before will be be same if there are more than one of that type
            count.extend([order.count(x)])
        n += 1

    return {'letter': letter, 'count': count}               #Returns a dictionary

if __name__ == '__main__':
    text = raw_input('Please enter text to analyse: ')

    freqs = letter_freq(text)

    """!!!   Have some problems with "print('{}').format(i)" command in general, so had to modify the last part   !!!"""
    for n in range(0, len(freqs['letter'])):
        print freqs['letter'][n] , '    ' , freqs['count'][n]       #Prints out letter and number used at same line